console.info("index was loaded");
/**
 * A quicker way of adding in the event listener to the button for click events
 * This will also automatically ensure that you clicked the specific button before running the code.
 * @param btnNumber number The button we are adding to (e.g. 0)
 * @param callback function The function to call back on click.
 */
const addEventToButton = function (btnNumber, callback) {
    document
        .getElementById('button' + btnNumber)
        .addEventListener('click', function (evt) {
            if (!event.target.matches('#button' + btnNumber)) return;
            callback.apply(this, evt);
        });
};

/**
 * Button 0 just simply displays an alert
 */
addEventToButton(0, function (event) {
    document.querySelector('#renderhere').innerHTML = '<p>Hello there! Nice to meet you!</p>'
});

/**
 * Button 1 creates a simple form that asks for your name and email address to "sign-up" for emails.
 * (Not really, but it's a simple example)
 */
addEventToButton(1, function (event) {
    const htmlFormElement = document.createElement('form');
    htmlFormElement.noValidate = false;
    htmlFormElement.name = 'btn1form';
    //There are two ways of adding HTML here. The first:
    htmlFormElement.innerHTML =
      "<p>Taking an interest in our weekly circular, <q>The gut speaks</q>? Enter your name and email address below to subscribe to email alerts of new issues, and exclusive member benefits!" +
      "<br /><small>We super promise to share your details with every other person we know and will definitely sell your data to Facebook.</small>"+
      "</p>"+
      "<div class='form-group'>"+
          "<label for='btn1name'>Name</label>"+
         " <input type='text' class='form-control' name='name' required='required' id='btn1name' placeholder='User McUserface' />"+
    "</div>"
    //Or the second:
    const formGroup = document.createElement('div');
    formGroup.classList.add('form-group');
    const htmlEmailInput = document.createElement('input');
    htmlEmailInput.type = 'email';
    htmlEmailInput.name = 'email';
    htmlEmailInput.autocomplete = 'off';
    htmlEmailInput.autofocus = 'autofocus';
    htmlEmailInput.id = 'btn1email';
    htmlEmailInput.classList.add('form-control');
    htmlEmailInput.placeholder = 'jbarlin2@une.edu.au';
    htmlEmailInput.required = 'required';
    htmlEmailInput.oninvalid = evt => {
        evt.target.setCustomValidity(evt.target.validity.valueMissing ? 'Please enter an email so my boss can get his cheque!' : '');
    };
    const htmlLabelElement = document.createElement('label');
    htmlLabelElement.for = 'btn1email';
    htmlLabelElement.innerText = 'Email Address';
    const htmlFormGroupElement = document.createElement('div');
    htmlFormGroupElement.classList.add('form-group');
    htmlFormGroupElement.append(htmlLabelElement);
    htmlFormGroupElement.append(htmlEmailInput);
    //Now build the submit button
    const submitBtn = document.createElement('button');
    submitBtn.innerText = 'Submit';
    submitBtn.classList.add('btn', 'btn-primary');
    submitBtn.type = 'submit';
    //And append the email address and submit button to the form
    htmlFormElement.append(htmlFormGroupElement);
    htmlFormElement.append(submitBtn);

    //OK, now we need to do something when the form is submitted...
    htmlFormElement.addEventListener(
        'submit',
        function (event) {
            event.preventDefault();
            const formData = new FormData(event.target);
            console.debug('I have downloaded the form!', {
                event,
                formData
            });
            document
                .getElementById('renderhere')
                .innerHTML = '<p>Thank you, <span data-output="name">' + formData.get('name') + '</span>, for signing up for our weekly emails at <span data-output="email">' + formData.get('email') + '</span></p>';
        }
    );
    document
        .getElementById('renderhere')
        .childNodes.forEach(value => value.remove());
    document
        .getElementById('renderhere')
        .append(htmlFormElement);
});

//Button 2
addEventToButton(2, function (event) {
    let randomNumber = Math.floor(Math.random()*20)+1
    let timesGuessed = 0
    let txt;
    let won = false;
    const htmlFormElement = document.createElement('form');
    //There are two ways of adding HTML here. The first:
    htmlFormElement.innerHTML =
      `<h5>Input a number between 1 and 20</h5> 
      <div class='form-group'>
          <label>Input</label>
          <input type='number' class='form-control' required='required' min='1' max='20' id='guessedNumber' placeholder='Enter your guess' />
          <small id="ans" class="form-text text-muted"> </small>
          <span id='guess' data-output='guesses'>Guesses: 0</span>
      </div> 
      <button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>`
      
    htmlFormElement.addEventListener("submit", function(event) {
      const guessedNumber = document.getElementById('guessedNumber').value;
      event.preventDefault();
      if (guessedNumber > randomNumber) { //Number too high
        txt = "too high";
        timesGuessed++;
      } else if (guessedNumber < randomNumber) { //Number too low
        txt = "too low";
        timesGuessed++;
      } else if (guessedNumber == randomNumber) { //Number equals original number to be guessed
        document.getElementById("renderhere").innerHTML = "You win!";
        won = true;
      } 
      if(!won) { //If guesses run out
        if(timesGuessed == 5) {
          document.getElementById('guess').innerHTML = "You have ran out of guesses!"
          document.getElementById("ans").innerHTML = "number I was thinking of was " + randomNumber;   
          document.getElementById('submitBtn').setAttribute('disabled', true)  
        } else { //If game not lost and guesses remains
          document.getElementById('ans').innerHTML = txt
          document.getElementById('guess').innerHTML = `Guesses: ${timesGuessed}`
        }
      }
      
      
    });
    document
        .getElementById('renderhere')
        .childNodes.forEach(value => value.remove());
    document
        .getElementById('renderhere')
        .append(htmlFormElement);
});

//Button 3 delete item
const deleteListItem = () => { //remove the elements
  let item;
  let parent = document.getElementById(`list-block`)
      let items = Array.prototype.slice.call(parent.childNodes);
      while (item = items.pop()) {
        if (item.firstChild && item.firstChild.checked) {
          parent.removeChild(item);
        }
    }
}   

//Button 3
addEventToButton(3, function (event) {
  const mainDiv = document.createElement('div');
  mainDiv.id = "main"
  const htmlFormElement = document.createElement('form')
  htmlFormElement.innerHTML = 
      `<div class="form-group">
        <input type="text" required class="form-control" placeholder="new task" id="task" name="task" />
      </div> 
      <button class="btn btn-primary" type="submit">Add</button> 
      <button class="btn btn-primary" type="button" onClick="deleteListItem()">Delete</button>
      <h1>To-do list</h1>`
  
  const toDoList = document.createElement('div');
  toDoList.id = "list-block"
  
  htmlFormElement.addEventListener("submit", function(event) {
    event.preventDefault();
    const formData = new FormData(event.target);
    document.getElementById('task').value = "";
    const listItem = document.createElement('div');
    listItem.classList.add('form-check')
    listItem.innerHTML =  `<input class="form-check-input" type="checkbox"> 
    <label class="form-check-label" for="defaultCheck1"> 
      ${formData.get('task')} 
    </label>` 
    toDoList.append(listItem)
  });
  document.getElementById('renderhere').childNodes.forEach(value => value.remove());
  mainDiv.append(htmlFormElement)
  mainDiv.append(toDoList)
  document.getElementById('renderhere').append(mainDiv);


});

//Button 4 
//Global letiables
let turnBoolean = false; //Naught turn O = true; Crosses turn X = false
let playerTurn = "Crosses turn!"
let c=0; //Total moves count
let gameEnded = false
const move = (id) => { //turns
  let cross = "X"
  let circle = "O" 
  if(document.getElementById(`${id}`).innerHTML === '-' && !gameEnded) { //Check if button already clicked
    c++;
    if(turnBoolean) {
      document.getElementById(`${id}`).innerHTML = circle
      turnBoolean = false
      document.getElementById(`turn`).innerHTML = "Crosses turn!"
    } else {
      document.getElementById(`${id}`).innerHTML = cross
      turnBoolean = true
      document.getElementById(`turn`).innerHTML = "Noughts turn!"
    }
    if(getWinner() == 'O won') {
      document.getElementById(`turn`).innerHTML = 'Noughts wins!'
      gameEnded = true
    } else if(getWinner() == 'X won') {
      document.getElementById(`turn`).innerHTML = 'Crosses wins!'
      gameEnded = true
    } else if(getWinner() == 'Draw' && c==9) {
      document.getElementById(`turn`).innerHTML = "Draw!"
      gameEnded = true
    }
  }
}

const getValue = (id) => { //Get the value of the button's given id
  return document.getElementById(`${id}`).innerHTML
}

const getWinner = () => { //Method to find the winner
  //Check if X won
  if(getValue('11') == 'X' && getValue('12') == 'X' && getValue('13') == 'X') {
    return "X won"
  } else if (getValue('21') == 'X' && getValue('22') == 'X' && getValue('23') == 'X') {
    return "X won"
  } else if (getValue('31') == 'X' && getValue('32') == 'X' && getValue('33') == 'X') {
    return "X won"
  } else if (getValue('11') == 'X' && getValue('21') == 'X' && getValue('31') == 'X') {
    return "X won"
  } else if (getValue('12') == 'X' && getValue('22') == 'X' && getValue('32') == 'X') {
    return "X won"
  } else if (getValue('13') == 'X' && getValue('23') == 'X' && getValue('33') == 'X') {
    return "X won"
  } else if (getValue('11') == 'X' && getValue('22') == 'X' && getValue('33') == 'X') {
    return "X won"
  } else if (getValue('13') == 'X' && getValue('22') == 'X' && getValue('31') == 'X') {
    return "X won"
  } 
  //Check if O won 
  else if(getValue('11') == 'O' && getValue('12') == 'O' && getValue('13') == 'O') {
    return "O won"
  } else if (getValue('21') == 'O' && getValue('22') == 'O' && getValue('23') == 'O') {
    return "O won"
  } else if (getValue('31') == 'O' && getValue('32') == 'O' && getValue('33') == 'O') {
    return "O won"
  } else if (getValue('11') == 'O' && getValue('21') == 'O' && getValue('31') == 'O') {
    return "O won"
  } else if (getValue('12') == 'O' && getValue('22') == 'O' && getValue('32') == 'O') {
    return "O won"
  } else if (getValue('13') == 'O' && getValue('23') == 'O' && getValue('33') == 'O') {
    return "O won"
  } else if (getValue('11') == 'O' && getValue('22') == 'O' && getValue('33') == 'O') {
    return "O won"
  } else if (getValue('13') == 'O' && getValue('22') == 'O' && getValue('31') == 'O') {
    return "O won"
  }  else {
    //No one won
    return "Draw"
  }

}

addEventToButton(4, function(event) {
  turnBoolean = false; //Naught turn O = true; Crosses turn X = false
  playerTurn = "Crosses turn!"
  c=0; //Total moves count
  gameEnded = false
  const container = document.createElement('div')
  container.classList.add('container');
  container.innerHTML = `
  <div class='row'>
    <div class='col-4'>
    <table class="table table-borderless">
      <tbody>
        <tr>
          <td style="margin: 0 4px 4px 0" type="button" id='11' onClick="move('11')" class="btn btn-outline-primary">-</td>
          <td style="margin: 0 4px 4px 0" type="button" id='12' onClick="move('12')" class="btn btn-outline-primary">-</td>
          <td style="margin: 0 4px 4px 0" type="button" id='13' onClick="move('13')" class="btn btn-outline-primary">-</td>
        </tr>
        <tr>
          <td style="margin: 0 4px 4px 0" type="button" id='21' onClick="move('21')" class="btn btn-outline-primary">-</td>
          <td style="margin: 0 4px 4px 0" type="button" id='22' onClick="move('22')" class="btn btn-outline-primary">-</td>
          <td style="margin: 0 4px 4px 0" type="button" id='23' onClick="move('23')" class="btn btn-outline-primary">-</td>
        </tr>
        <tr>
          <td style="margin: 0 4px 4px 0" type="button" id='31' onClick="move('31')" class="btn btn-outline-primary">-</td>
          <td style="margin: 0 4px 4px 0" type="button" id='32' onClick="move('32')" class="btn btn-outline-primary">-</td>
          <td style="margin: 0 4px 4px 0" type="button" id='33' onClick="move('33')" class="btn btn-outline-primary">-</td>
        </tr>
      </tbody>
    </table>
    </div>
    <div class='row'>
      <h5 id='turn'>Crosses turn!<h5> 
    </div>
  </div>`
document.getElementById('renderhere').childNodes.forEach(value => value.remove());
document.getElementById('renderhere').innerHTML = "" //Empty the renderhere div
  document.getElementById('renderhere').append(container);
});

//Button 5 
const generateCanvas = () => {
  return `
    <canvas id="canvas" width="300px" height="300px" style="border: 1px solid black;"/>
  `;
}

addEventToButton(5, function (event) {
  document
      .getElementById('renderhere')
      .childNodes.forEach(value => value.remove());
  
    document.getElementById('renderhere').innerHTML = generateCanvas();
    let ctx = document.getElementById("canvas").getContext("2d");
    
    let _x = 0;
    let _y = 0;
    let drag = false;
    document.getElementById("canvas").addEventListener('mousemove', function(e) {
      ctx.moveTo(_x, _y);
      if (drag) {
        ctx.lineTo(e.offsetX, e.offsetY);
        ctx.stroke();
      }
      _x = e.offsetX;
      _y = e.offsetY;
    });

    document.getElementById("canvas").addEventListener('mousedown', function() {
      drag = true;
    });

    document.getElementById("canvas").addEventListener('mouseup', function() {
      drag = false;
    });
});

//Button 6 
const getSinSVG = () => {
  let path = "";
  let y = 0;
  for (let i = 0; i < 2 * Math.PI; i += 0.01) {
    path += `<circle cx="${Math.sin(i) * 300}" cy="${y}" r="1" fill="red"/>`;
    y += 0.945;
  }
  return `<div style="border: 1px solid black; width: 300px; height: 300px;">
    <svg version="1.1"
      baseProfile="full"
      width="300" height="300"
      xmlns="http://www.w3.org/2000/svg">
      ${path}
    </svg>
  </div>`;
}

addEventToButton(6, function (event) {
  document
      .getElementById('renderhere')
      .childNodes.forEach(value => value.remove());
  document.getElementById('renderhere').innerHTML = getSinSVG();
});

//Button 7
let button7html = null;
let button7value = 0;
let button7y = 0;
let button7path = "";
let buttonX = 0;
let buttonT = 0;
let button7interval = null;

const  resetButton7 = () => {
  button7y = 0;
  buttonT = 0;
  button7path = "";
}

function updateGraph(e) {
  button7value = e.target.value;
  resetButton7();
}

// Game loop
function generateHtml() {
  // If we have shifted to another button, clear this.
  if (!document.getElementById('animatedSVG')) {
    clearInterval(button7interval);
    return;
  }
  buttonT = (new Date()).getTime() / 1000;
  button7path += `<circle cx="${(Math.sin(button7value + buttonT) * 150) + 150}" cy="${button7y}" r="1" fill="red"/>`
  button7y += 0.945;
  if (button7y > 300) {
    resetButton7();
  }
  document.getElementById('animatedSVG').innerHTML = `<div>
    <div style="border: 1px solid black; width: 300px; height: 300px;">
    <svg version="1.1"
      baseProfile="full"
      width="300" height="300"
      xmlns="http://www.w3.org/2000/svg">
      ${button7path}
    </svg>
  </div>
  </div>`
}
function getAnimatedSvg() {
  resetButton7();
  button7interval = setInterval(function() {
    generateHtml();
  }, 33);
}

addEventToButton(7, function (event) {
  document
      .getElementById('renderhere')
      .childNodes.forEach(value => value.remove());
  
  document.getElementById('renderhere').innerHTML = `
    <div>
      <label>Sin Graph Modifier</label>
      <input id="button7value" type="number" value="0" />
    </div>
    <div id="animatedSVG"></div>
  `;
  document.getElementById('button7value').addEventListener('input', updateGraph);
  getAnimatedSvg();
});

/* Button 8 */
// ### Button 8: Mastermind

// Users are able to submit a sequence they have submitted before (although that's at their detriment).
var mastermindColours = [
  'Xanadu', 'Arsenic', 'Fallow',
  'Gamboge', 'Niagara', 'Cerise'
];
var hardCodedCode = getRandomCode() // ['Xanadu', 'Arsenic', 'Niagara', 'Arsenic'];
var mastermindColoursSingle = mastermindColours.map(function(m) { return m.slice(0,1) })
var button8defaultState = {
  code: hardCodedCode, // getRandomCode(),
  guesses: [],
  turnsRemaining: 8,
  win: false,
  lose: false
};

var button8state = {
  code: hardCodedCode, //getRandomCode(),
  guesses: [],
  turnsRemaining: 8,
  win: false,
  lose: false
};

function updateButton8state(obj) {
  button8state = { ...button8state, ...obj};
  generateMasterMind();
}
function getRandomCode() {
  return [1,2,3,4].map(function(code) {
    return mastermindColours[Math.floor(Math.random() * 6)];
  });
}
function guess(guessArray) {
  var code = Array.from(button8state.code); // XA-- // AN-- // EEBB
  var gg = Array.from(guessArray);
  var result = ['E', 'E', 'E', 'E'];

  // First get all the right colors
  guessArray.forEach(function(g, i) {
    if (code[i] === guessArray[i]) {
      result[i] = 'B';
      guessArray[i] = '6'; // Random value, doesn't mean anyything
      code[i] = '7'; // Random value, doesn't mean anyything
    }
  });

  guessArray.forEach(function(g, i) {
    code.forEach(function(c, j) {
      if (code[j] === guessArray[i]) {
        result[i] = 'W';
        code[j] = '7';
        guessArray[i] = '6';
      }
    })
  })

  // Did we win?
  var win = result.filter(function(r) { return r === 'B' }).length === 4;
  var lose = false;
  // If we didn't win, and this was our last turn, means we lost :(
  if (!win) {
    lose = button8state.turnsRemaining === 1;
  }

  var guesses = [];
  // if (!lose && !win) {
    guesses = button8state.guesses;
    guesses.push({
      guess: gg.join('-'),
      result: result.join('-')
    });
  // }
  updateButton8state({ 
    guesses,
    turnsRemaining: button8state.turnsRemaining - 1,
    win,
    lose
  });
}
function formatCode(code) {
  return code.split('-').map(function(g) {
    return g.slice(0, 1);
  }).join('-');
}
function generateMasterMind() {
  var options = mastermindColours.map(function(m) {
    return `<option value="${m}">${m}</option>`;
  }).join('');
  document.getElementById('renderhere').innerHTML = `<div>
    <h1>Welcome to master mind, try to guess my code</h1>
    <form id="masterMindForm">
      <select name="guess1">
        ${options}
      </select>
      <select name="guess2">
        ${options}
      </select>
      <select name="guess3">
        ${options}
      </select>
      <select name="guess4">
        ${options}
      </select>
      <button id="submitButton" type="submit">Submit</button>
      ${button8state.turnsRemaining > 0 ? `<h2>Turns remaining: ${button8state.turnsRemaining}</h2>`: ''}
      ${button8state.win ? `<h3>You win!</h3>` : ''}
      ${button8state.lose ? `<h3>You fool: my code was ${formatCode(button8state.code.join('-'))}</h3>` : ''}
    </form>
    <ol>
      ${button8state.guesses.map(function(guess) {
        return `<li>You guessed ${formatCode(guess.guess)} (Result: ${guess.result})</li>`;
      }).join('')}
    </ol>
    
  </div>`;
  var lastGuess = button8state.guesses.length ? 
    button8state.guesses[button8state.guesses.length - 1].guess.split('-') : 
    null;
  
  if (lastGuess) {
    Array.from(document.getElementsByTagName('select')).forEach(function(s,i){
      s.selectedIndex = mastermindColours.indexOf(lastGuess[i]);
    })
  }

  if (button8state.win || button8state.lose) {
    Array.from(document.getElementsByTagName('select')).forEach(function(s,i){
      s.disabled = true;
    })
    document.getElementById("submitButton").disabled = true;
  }

  if (button8state.lose){
    Array.from(document.getElementsByTagName('select')).forEach(function(s,i){
      s.selectedIndex = mastermindColours.indexOf(button8state.code[i]);
    })
  }

  document.getElementById('masterMindForm').addEventListener(
    'submit',
    function (event) {
        event.preventDefault();
        const formData = new FormData(event.target);
        var guessArray = []
        for (var [key, value] of formData.entries()) { 
          guessArray.push(value);
        };
        guess(guessArray);
    }
  );
}
addEventToButton(8, function (event) {
  document
      .getElementById('renderhere')
      .childNodes.forEach(value => value.remove());
  button8state = JSON.parse(JSON.stringify(button8defaultState))
  generateMasterMind();
});

//Button 9
const tbody = document.createElement('tbody'); //Create table body
const tbodyCart = document.createElement('tbody'); //Create cart table body
tbodyCart.id = "cart"
const fruits = () => {
  let item;
  let parent = document.getElementsByTagName('tbody')[0] //Delete table contents
      let items = Array.from(parent.childNodes);
      while (item = items.pop()) {
          parent.removeChild(item);        
    }
  document.getElementById('fruitBtn').setAttribute('disabled', true); //Disable and enable buttons
  document.getElementById('biscuitBtn').removeAttribute('disabled');
  document.getElementById('bakeryBtn').removeAttribute('disabled');
  grocery_shop.aisles[0].items.map((item,i) => { //Render table contents
    const tr = document.createElement('tr')
    tr.innerHTML = `
    <th scope="row">${(i+1)}</th>
    <td>${item.name}</td>
    <td>${item.cost}</td>
    <td>${item.per}</td>
    <td><button type="button" class="btn btn-primary" onClick="addToCart('${item.name}', ${item.cost}, '${item.per}')">Add to Cart</button></td>`
    tbody.append(tr) //Append rows to tables
  })
}
const bakery = () => {
  let item;
  let parent = document.getElementsByTagName('tbody')[0] //Delete table contents
      let items = Array.from(parent.childNodes);
      while (item = items.pop()) {
          parent.removeChild(item);        
    }
  document.getElementById('fruitBtn').removeAttribute('disabled'); //Disable and enable buttons
  document.getElementById('biscuitBtn').removeAttribute('disabled');
  document.getElementById('bakeryBtn').setAttribute('disabled', true);
  grocery_shop.aisles[1].items.map((item,i) => { //Render table contents
    const tr = document.createElement('tr')
    tr.innerHTML = `
    <th scope="row">${(i+1)}</th>
    <td>${item.name}</td>
    <td>${item.cost}</td>
    <td>${item.per}</td>
    <td><button type="button"  class="btn btn-primary" onClick="addToCart('${item.name}', ${item.cost}, '${item.per}')">Add to Cart</button></td>`
    tbody.append(tr) //Append rows to tables
  })
}
const biscuits = () => {
  let item;
  let parent = document.getElementsByTagName('tbody')[0] //Delete table contents
      let items = Array.from(parent.childNodes);
      while (item = items.pop()) {
          parent.removeChild(item);        
    }
  document.getElementById('fruitBtn').removeAttribute('disabled'); //Disable and enable buttons
  document.getElementById('biscuitBtn').setAttribute('disabled', true);
  document.getElementById('bakeryBtn').removeAttribute('disabled');
  grocery_shop.aisles[2].items.map((item,i) => { //Render table contents
    const tr = document.createElement('tr')
    tr.innerHTML = `
    <th scope="row">${(i+1)}</th>
    <td>${item.name}</td>
    <td>${item.cost}</td>
    <td>${item.per}</td>
    <td><button type="button" class="btn btn-primary" onClick="addToCart('${item.name}', ${item.cost}, '${item.per}')">Add to Cart</button></td>`
    tbody.append(tr) //Append rows to tables
  })
}

let cartItems = { //Create JSON of cart items
  cartProducts: []
}

let totalCost = 0
let Quant = 0

const addToCart = (name, cost, per) => { //Function to add items to cart
  totalCost += cost
  let children;
  let parent = document.getElementById(`cart`) //Delete cart items
      let items = Array.prototype.slice.call(parent.childNodes);
      while (children = items.pop()) {
          parent.removeChild(children);      
    }
  let found = false //boolean; false if array doesnt have item already
    let item = {name: name, Quant: Quant+1, cost: cost, per:per} //Item object to be added
    
    cartItems.cartProducts.map(product => { //method to check if cart already has items
      if(product.name == name) {
        product.Quant++ //add quantity if yes
        found = true;
      }
    })
    if(!found) {
      Quant = 0;
      cartItems.cartProducts.push(item) //Push item if not already included
    }
    cartItems.cartProducts.map(product => { //Render cart tables with updated information
      const tr = document.createElement('tr') //Create table tow
      tr.innerHTML = `
    <td>${product.name}</td>
    <td>${product.Quant}</td>
    <td>$${product.cost}/${product.per}</td>`
    tbodyCart.append(tr) //Appned to cart
    })
    const tr1 = document.createElement('tr') //Create table row for cost
    tr1.innerHTML = `
    <td>Total Cost: <strong>$${parseFloat(Math.round(totalCost * 100) / 100).toFixed(2)}</strong></td>`
    tbodyCart.append(tr1)
}

addEventToButton(9, function (event) {
  const mainDiv = document.createElement('div'); //Create main div
  mainDiv.id = "main"

  const container = document.createElement('div'); //Create container
  container.classList.add('container')

  const row = document.createElement('div'); //Create row
  row.classList.add('row')

  const col6 = document.createElement('div'); //Create column 6 div
  col6.classList.add('col-6')

  const col4 = document.createElement('div'); //Crete column 4 div
  col4.classList.add('col-6')

  container.append(row) //Append row in container
  row.append(col6) //Appned col 6 in row
  row.append(col4) //Append col 4 in row
  

  //Menu items
  const table = document.createElement('table')
  table.classList.add('table');
  table.innerHTML = 
  `<thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Cost</th>
      <th scope="col">Per</th>
      <th scope="col">Add</th>
    </tr>
  </thead>`
  table.append(tbody)

  //Cart
  const cartTable = document.createElement('table')
  cartTable.classList.add('table');
  cartTable.classList.add('table-borderless');
  cartTable.innerHTML = `
  <thead>
    <tr>
      <th scope="col">Item</th>
      <th scope="col">Quntaity</th>
      <th scope="col">Cost</th>
    </tr>
  </thead>`
  cartTable.append(tbodyCart) //Append cart body to cart table
  const buttons = document.createElement('div'); //Create div for buttons
  buttons.id = "buttons"
  buttons.innerHTML = `
  <div>
  <h1>Welcome to the Market</h1>
  <p>Please select an aisle to do shopping</p>
  </div>
  <button type="button" id="fruitBtn" class="btn btn-primary" onClick="fruits()">${grocery_shop.aisles[0].name}</button>
  <button type="button" id="bakeryBtn" class="btn btn-primary" onClick="bakery()">${grocery_shop.aisles[1].name}</button>
  <button type="button" id="biscuitBtn" class="btn btn-primary" onClick="biscuits()">${grocery_shop.aisles[2].name}</button>`
  
  document.getElementById('renderhere').childNodes.forEach(value => value.remove());
  col6.append(table) //Append table to col 6 div
  col4.append(cartTable) //Append carttable to col 4 div
  mainDiv.append(buttons) //Append buttons to main div
  mainDiv.append(container) //Append container to main div
  document.getElementById('renderhere').innerHTML = "" //Set renderhere to empty
  document.getElementById('renderhere').append(mainDiv);  //Append main div to renderhere


});
